from distutils.core import setup

setup(
    name='dwHelpers',
    version='0.1',
    packages=["openpyxl_extensions"],
    url='',
    license='',
    author='dwinter',
    author_email='dwinter@mpiwg-berlin.mpg.de',
    description=''
)
