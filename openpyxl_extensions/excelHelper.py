
from logging import getLogger

logger = getLogger()

class SheetHandler(object):
    def __init__(self,sheet):
        self.sheet = sheet
        
        #get  list of all variables used
        self.names = [x.value for x in list(sheet.rows)[0]]
        self.nm={}
        for x in self.names:
            self.nm[x]=self.names.index(x)
            
            
    def getV(self,row,name):
        number = self.nm[name]
        if row[number].value is not None:
            return ("%s"%row[number].value).strip()
        return ""
    
    
    def rowToNode(self,row,ng,nodeID,mapping,params={}):
        
        for key,value in mapping.items():
            try:
                params[key]=self.getV(row, value)
            except KeyError:
                logger.debug("No value: %s"%value)
        
        params["nid"]=self.getV(row, nodeID)
        ng.add_node(self.getV(row, nodeID),**params)
        
    def rowToEdge(self,row,ng,source,target,mapping,params={}):
        
        
        for key,value in mapping.items():
            params[key]=self.getV(row, value)
        
        ng.add_edge(self.getV(row,source),self.getV(row,target),**params)
        
        
    
        
    
        
        
                
        